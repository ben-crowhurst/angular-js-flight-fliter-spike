var app = angular.module('app', []);

function FlightResultsController($scope, $http) {
  $scope.private = {
    initialise_flight_stop_components: function() {
      $scope.StopList = _.uniq(_.flatten(_.map($scope.results, function(result) {
        return _.isObject(result.Inbound) ? [result.Inbound.Stops, result.Outbound.Stops] : [result.Outbound.Stops];
      })));

      $scope.stopCheckboxes = _.object($scope.StopList, _($scope.StopList.length).times(function(){return true}));
    },
    initialise_flight_airline_components: function() {
      $scope.AirlineList = _.uniq(_.flatten(_.map($scope.results, function(result) {
        return _.isObject(result.Inbound) ? [result.Inbound.AirlineName, result.Outbound.AirlineName] : [result.Outbound.AirlineName];
      })));

      $scope.airlineCheckboxes = _.object($scope.AirlineList, _($scope.AirlineList.length).times(function(){return true}));
    },
    initialise_flight_duration_components: function() {
      var private = this;
      var durations = _.uniq(_.map($scope.results, function(result) {
        return result.Outbound.Duration;
      }));

      $scope.DurationMinimum = private.convert_milliseconds_to_seconds(_.min(durations));
      $scope.DurationMaximum = private.convert_milliseconds_to_seconds(_.max(durations));
      $scope.SelectedDurationMinimum = $scope.DurationMinimum;
      $scope.SelectedDurationMaximum = $scope.DurationMaximum;
    },
    initialise_flight_departure_time_components: function() {
      var private = this;
      private.initialise_outbound_departure_time_components();
      private.initialise_inbound_departure_time_components();
    },
    initialise_outbound_departure_time_components: function() {
      var private = this;
      var outbound_departure_times = _.uniq(_.map($scope.results, function(result) {
        return result.Outbound.DepartureTime;
      }));

      $scope.OutboundDepartureTimeMinimum = private.convert_milliseconds_to_seconds(_.min(outbound_departure_times));
      $scope.OutboundDepartureTimeMaximum = private.convert_milliseconds_to_seconds(_.max(outbound_departure_times));
      $scope.SelectedOutboundDepartureTimeMinimum = $scope.OutboundDepartureTimeMinimum;
      $scope.SelectedOutboundDepartureTimeMaximum = $scope.OutboundDepartureTimeMaximum;
    },
    initialise_inbound_departure_time_components: function() {
      var private = this;
      var inbound_departure_times = _.uniq(_.map($scope.results, function(result) {
        return _.isObject(result.Inbound) ? result.Inbound.DepartureTime : 0;
      }));
      inbound_departure_times = _.without(inbound_departure_times, 0);

      $scope.InboundDepartureTimeMinimum = private.convert_milliseconds_to_seconds(_.min(inbound_departure_times));
      $scope.InboundDepartureTimeMaximum = private.convert_milliseconds_to_seconds(_.max(inbound_departure_times));
      $scope.SelectedInboundDepartureTimeMinimum = $scope.InboundDepartureTimeMinimum;
      $scope.SelectedInboundDepartureTimeMaximum = $scope.InboundDepartureTimeMaximum;
    },
    has_matching_stops: function(result) {
      var private = this;
      var status = false;
      var range = private.build_range($scope.stopCheckboxes);

      if (range.indexOf(result.Outbound.Stops.toString()) > -1) {
        if (_.isObject(result.Inbound) && range.indexOf(result.Inbound.Stops.toString()) === -1) {
          status = false;
        } else {
          status = true;
        }
      }

      return status;
    },
    has_matching_airline: function(result) {
      var private = this;
      var status = false;
      var range = private.build_range($scope.airlineCheckboxes);

      if (range.indexOf(result.Outbound.AirlineName) > -1) {
        if (_.isObject(result.Inbound) && range.indexOf(result.Inbound.AirlineName) === -1) {
          status = false;
        } else {
          status = true;
        }
      }

      return status;
    },
    has_matching_outbound_flight_duration: function(result) {
      var private = this;
      var status = false;
      var minimum = $scope.SelectedDurationMinimum;
      var maximum = $scope.SelectedDurationMaximum;
      var duration = private.convert_milliseconds_to_seconds(result.Outbound.Duration);

      if (duration >= minimum && duration <= maximum) {
        status = true;
      }

      return status;
    },
    has_matching_outbound_departure_time: function(result) {
      var private = this;
      var status = false;
      var departure_time = private.convert_milliseconds_to_seconds(result.Outbound.DepartureTime);
      var minimum = $scope.SelectedOutboundDepartureTimeMinimum;
      var maximum = $scope.SelectedOutboundDepartureTimeMaximum;

      if (departure_time >= minimum && departure_time <= maximum) {
        status = true;
      }

      return status;
    },
    has_matching_inbound_departure_time: function(result) {
      var private = this;
      var status = false;
      var departure_time = private.convert_milliseconds_to_seconds(result.Inbound.DepartureTime);
      var minimum = $scope.SelectedInboundDepartureTimeMinimum;
      var maximum = $scope.SelectedInboundDepartureTimeMaximum;

      if (departure_time >= minimum && departure_time <= maximum) {
        status = true;
      }

      return status;
    },
    build_range: function(object) {
      var range = [];

      for (var key in object) {
        if (object[key]) {
          range.push(key);
        }
      }

      return range;
    },
    convert_milliseconds_to_seconds: function(value) {
      return  Math.round(value / (1000 * 60 * 60));
    }
  };

  $http.get('results.json').success(function(data, status, headers, config) {
    $scope.results = data;
    $scope.sortOrder = "";
  }).then(function(data) {
    $scope.private.initialise_flight_stop_components();
    $scope.private.initialise_flight_airline_components();
    $scope.private.initialise_flight_departure_time_components();
    $scope.private.initialise_flight_duration_components();
  });

  $scope.searchFilter = function(result) {
    var stop_matched = $scope.private.has_matching_stops(result);
    var airline_matched = $scope.private.has_matching_airline(result);
    var flight_duration_matched = $scope.private.has_matching_outbound_flight_duration(result);
    var outbound_flight_departure_time_matched = $scope.private.has_matching_outbound_departure_time(result);
    var inbound_flight_departure_time_matched = _.isObject(result.Inbound) ? $scope.private.has_matching_inbound_departure_time(result) : true;

    return stop_matched &&
           airline_matched &&
           flight_duration_matched &&
           inbound_flight_departure_time_matched &&
           outbound_flight_departure_time_matched;
  };
}
